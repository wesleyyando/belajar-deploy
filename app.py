from flask import Flask
import os

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello World from Wesley Yando Tantra'

if __name__=='__main__':
    app.run(host='0.0.0.0', port=os.environ.get('PORT', 5000))